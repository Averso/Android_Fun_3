package com.franke.savingdata;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static EditText textInput;
    private static TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setting toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);

        //set title for activity
        setTitle(getString(R.string.save_sets_title));

        //get widgets
        textInput = (EditText) findViewById(R.id.editTextInput);
        textView = (TextView) findViewById(R.id.textViewSaved);

        //open saved text
        SharedPreferences sharedPref = this.getSharedPreferences( getString(R.string.pref_file_key), Context.MODE_PRIVATE);
        String defaultText = getResources().getString(R.string.saved_string_default);
        //we pass default value to compare
        String text = sharedPref.getString(getString(R.string.saved_string),defaultText);

        textView.setText(text);

    }

    //TODO:handle "enter" as save file too
    public void saveText()
    {   View view = this.getCurrentFocus();
        if(view!=null)
        {
            //hide keyboard after touching save button
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            Context context = getApplicationContext();

            //getting text from editText
            String textToSave = textInput.getText().toString();

            if(!textToSave.isEmpty())
            {
                //getting handle to shared preferences

                //line below - when we've got multiple files, we use key to identify them
                SharedPreferences sharedPref = context.getSharedPreferences( getString(R.string.pref_file_key), Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.saved_string),textToSave);
                //editor.commit();
                editor.apply();

                Toast.makeText(context,getString(R.string.saved_info),Toast.LENGTH_LONG).show();
                //set input text to textView
                //textView.setText(textToSave);
            }
            else //if user hasn't entered any text
            {
                Toast.makeText(context,getString(R.string.empty_input_warning),Toast.LENGTH_LONG).show();
            }


        }




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.action_save:
                saveText();
                return true;
            case R.id.action_switch_save_set:
                return true;
            case R.id.action_switch_save_file:
                Intent intent = new Intent(this,SaveFileActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_switch_save_sql:
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
